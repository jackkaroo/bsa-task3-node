const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();
// TODO: Implement route controllers for fighter

router.get('/', function(req,res,next){
    res.data = FighterService.searchAllFighters();
    next();
  }, responseMiddleware)
  
  router.get('/:id',function(req,res,next){
    res.data = FighterService.searchFighter({id: req.params.id});
    next();
  },responseMiddleware)
  
  router.post('/',createFighterValid, function(req,res,next){
    if(!res.err){ 
    res.data = FighterService.createFighter(res.fighterValid);
    }
    next()
  },responseMiddleware)
  
  router.put('/:id', updateFighterValid, function(req,res,next){
    if(!res.err){
      res.data = FighterService.updateFighter(req.params.id, res.fighterValid);
    }
    next()
  },responseMiddleware)
  
  router.delete('/:id', function(req,res,next){
    res.data = FighterService.deleteFighter(req.params.id);
    if(res.data.length === 0) res.data=null;
    next();
  },responseMiddleware)

module.exports = router;