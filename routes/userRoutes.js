const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();
// TODO: Implement route controllers for user

router.get('/', function(req,res,next){
  res.data = UserService.searchAllUsers();
  next();
}, responseMiddleware)

router.get('/:id',function(req,res,next){
  res.data = UserService.search({id: req.params.id});
  next();
},responseMiddleware)

router.post('/',createUserValid, function(req,res,next){
  if(!res.err){ 
  res.data = UserService.createUser(res.userValid);
  }
  next()
},responseMiddleware)

router.put('/:id', updateUserValid, function(req,res,next){
  if(!res.err){ 
    res.data = UserService.updateUser(req.params.id, res.userValid);
  }
  next()
},responseMiddleware)

router.delete('/:id', function(req,res,next){
  res.data = UserService.deleteUser(req.params.id);
  if(res.data.length === 0) res.data=null;
  next();
},responseMiddleware)

module.exports = router;