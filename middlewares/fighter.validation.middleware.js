const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const errors = [];
    const { name, health, power, defense, ...rest } = req.body;

    if(Object.keys(rest).length !== 0) 
      errors.push('Something unnecessary was used.');

    if(!name|| !health || !power || !defense )
      errors.push('Not enought properties')
    else {
      if(!validateName(name)) errors.push('Name can not be empty.');
      else if(!validateUnicName(name)) errors.push('Fighter with this name already exists.')
      if(!validateHealth(health)) errors.push('Health should be 100.');
      if(!validatePower(power)) errors.push('Power validation error.');
      if(!validateDefense(defense)) errors.push('Defense validation error.');

      else if(errors.length===0) 
        res.fighterValid = {name, health, power, defense};
  }
    
    if(errors.length!=0) res.err = errors;
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update

    const errors = [];
    const { name, health, power, defense, ...rest } = req.body;

    if(Object.keys(rest).length !== 0) 
      errors.push('Something unnecessary was used.');

    if(!name|| !health || !power || !defense )
      errors.push('Not enought properties')
    else {
      if(!validateName(name)) errors.push('Name can not be empty.');
      else if(!validateUnicName(name)) errors.push('Fighter with this name already exists.')
      if(!validateHealth(health)) errors.push('Health should be 100.');
      if(!validatePower(power)) errors.push('Power validation error.');
      if(!validateDefense(defense)) errors.push('Defense validation error.');

      else if(validateUnicName(name) && validateName(name) && validateHealth(health) && validatePower(power) && validateDefense(defense)) 
      res.fighterValid = {name, health, power, defense};
  }
    
    if(errors.length!=0) res.err = errors;
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;

function validateName(inputText){
    return inputText.length && inputText.trim();
}

function validateUnicName(inputText){
  const fighters = FighterService.searchAllFighters();

  for(let i=0;i<fighters.length;i++){
    if(fighters[i].name && fighters[i].name.toLowerCase()===inputText.toLowerCase()) return 0;
  }
  return 1;
}

function validateHealth(inputInt){
    return inputInt === 100;
}

function validatePower(inputInt){
    return (inputInt>=0 && inputInt<100);
}

function validateDefense(inputInt){
    return (inputInt>0 && inputInt<=10);
}