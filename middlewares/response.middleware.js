const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
   if(res.data && !res.err) res.status(200).json(res.data);
   else if(!res.data && res.err) {
     res.status(400).json({
     error: true,
     message: res.err
    });
   }
   else if(!res.data && !res.err) res.status(404).json({
    error: true,
    message: 'Not found'
   });
   next();
}

exports.responseMiddleware = responseMiddleware;