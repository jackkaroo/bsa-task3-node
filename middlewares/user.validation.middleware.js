const { user } = require('../models/user');
const UserService = require('../services/userService');
const createUserValid = (req, res, next) => {
   // TODO: Implement validatior for user entity during creation
   
    const errors = [];
    const { firstName, lastName, email, phoneNumber, password, ...rest } = req.body;

    if(Object.keys(rest).length !== 0) 
      errors.push('Something unnecessary was used.');

    if(!firstName|| !lastName || !email|| !phoneNumber || !password )
      errors.push('Not enought properties');
    else {
      if(!validateFirstName(firstName)) errors.push('First name can not be empty.');
      if(!validateLastName(lastName)) errors.push('Last name can not be empty.');

      if(!validateEmail(email)) errors.push('Email validation error.');
      else if(!validateUnicEmail(email)) errors.push('User with this email already exists.')

      if(!validatePhone(phoneNumber)) errors.push('Phone number validation error.');
      else if(!validateUnicPhone(phoneNumber)) errors.push('User with this phone already exists.');

      if(!validatePassword(password)) errors.push('Password validation error.');

      else if(errors.length===0) 
        res.userValid = {firstName, lastName, email, phoneNumber, password};
  }
    
    if(errors.length!=0) res.err = errors;
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const errors = [];
    const { firstName, lastName, email,phoneNumber,password, ...rest } = req.body;

    if(Object.keys(rest).length !== 0) 
      errors.push('Something unnecessary was used.');

    if(!firstName|| !lastName || !email|| !phoneNumber || !password )
      errors.push('Not enought properties')
    else {
      if(!validateFirstName(firstName)) errors.push('First name can not be empty.');
      if(!validateLastName(lastName)) errors.push('Last name can not be empty.');

      if(!validateEmail(email)) errors.push('Email validation error.');
      else if(!validateUnicEmail(email)) errors.push('User with this email already exists.')
    
      if(!validatePhone(phoneNumber)) errors.push('Phone number validation error.');
      else if(!validateUnicPhone(phoneNumber)) errors.push('User with this phone already exists.');
      if(!validatePassword(password)) errors.push('Password validation error.');

      else if(validateFirstName(firstName) && validateLastName(lastName) && validateUnicPhone(email) && validateEmail(email) && validateUnicPhone(phoneNumber) && validatePhone(phoneNumber) && validatePassword(password)) 
        res.userValid = {firstName, lastName, email, phoneNumber, password};
  }
    
    if(errors.length!=0) res.err = errors;
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

function validateFirstName(inputText){
  return inputText.length && inputText.trim();
}

function validateLastName(inputText){
  return inputText.length && inputText.trim();
}

function validateEmail(inputText){
  if(inputText.length===0) return 0;
  let mailformat = /.+@gmail\.com$/;
  return inputText.match(mailformat);
}

function validateUnicEmail(inputText){
  const users = UserService.searchAllUsers();
  inputText = inputText.toLowerCase();

  for(let i=0;i<users.length;i++){
    if(users[i].email && users[i].email.toLowerCase()===inputText.toLowerCase()) return 0;
  }
  return 1;
}

function validatePhone(inputText){
  if(inputText.length===0) return 0;

  let phoneformat = /^[+]3?8?(0\d{9})$/;
  return inputText.match(phoneformat);
}

function validateUnicPhone(inputText){
  const users = UserService.searchAllUsers();

  for(let i=0;i<users.length;i++){
    if(users[i].phoneNumber===inputText) return 0;
  }
  return 1;
}

function validatePassword(inputText){
  return inputText.length>=3;
}