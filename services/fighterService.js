const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    searchFighter(id) {
        const item = FighterRepository.getOne(id);
        if(!item) {
            return null;
        }
        return item;
    }

    searchAllFighters() {
        const items = FighterRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    createFighter(data){
        const item = FighterRepository.create(data);
        if(!item) {
            return null;
        }
        return item;
    }

    updateFighter(id, dataToUpdate){
        const item = FighterRepository.update(id, dataToUpdate);
        if(!item) {
            return null;
        }
        return item;
    }

    deleteFighter(id){
        //const item = FighterRepository.getOne(id);
        const itemToDelete = FighterRepository.delete(id);
        if(!itemToDelete) {
            return null;
        }
        return itemToDelete;
    }
}

module.exports = new FighterService();