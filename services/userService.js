const { UserRepository } = require('../repositories/userRepository');

class UserService {
    // TODO: Implement methods to work with user
    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    searchAllUsers() {
        const items = UserRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    createUser(data){
        const item = UserRepository.create(data);
        if(!item) {
            return null;
        }
        return item;
    }

    updateUser(id, dataToUpdate){
        const item = UserRepository.update(id, dataToUpdate);
        if(!item) {
            return null;
        }
        return item;
    }

    deleteUser(id){
        const itemToDelete = UserRepository.delete(id);
        if(!itemToDelete) {
            return null;
        }
        return itemToDelete;
    }
}

module.exports = new UserService();
